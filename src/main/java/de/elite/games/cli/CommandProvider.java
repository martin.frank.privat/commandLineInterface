package de.elite.games.cli;

public interface CommandProvider {

    CommandList getCommands();

}
